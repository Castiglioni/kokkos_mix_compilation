#include <Kokkos_Core.hpp>

namespace kk_wrap{

void initialize(int argc, char *argv[])
{
   Kokkos::initialize(argc, argv);
}
void finalize()
{
   Kokkos::finalize();
}

}

