#
# Add library that depends on Kokkos.
# If Kokkos uses CUDA it requires special handling
# in order to compile with nvcc
#
function(quicc_add_library_kokkos libname)
  # check precondition
  # QuICC::Kokkos::kokkos target must be defined
  if(NOT TARGET QuICC::Kokkos::kokkos)
    message(FATAL_ERROR "Kokkos was not setup")
  endif()


  # parse inputs
  # cmake_parse_arguments(QAKL "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN})

  message(DEBUG "quicc_add_library_kokkos")
  list(APPEND CMAKE_MESSAGE_INDENT "${QUICC_CMAKE_INDENT}")
  # message(DEBUG "QAKL_SRC: ${QAKL_SRC}")
  message(DEBUG "libname: ${libname}")
  message(DEBUG "ARGN: ${ARGN}")

  message(DEBUG "Kokkos_ENABLE_CUDA: ${Kokkos_ENABLE_CUDA}")
  if(Kokkos_ENABLE_CUDA)
    set_source_files_properties(${ARGN} PROPERTIES LANGUAGE CUDA)
  endif()
  add_library(${libname} STATIC ${ARGN})
  target_link_libraries(${libname} PRIVATE QuICC::Kokkos::kokkos)


  list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction(quicc_add_library_kokkos)

#
# Add sources to existing library that depends on Kokkos.
#
function(quicc_target_sources_kokkos libname)

  message(DEBUG "quicc_target_sources_kokkos")
  list(APPEND CMAKE_MESSAGE_INDENT "${QUICC_CMAKE_INDENT}")
  message(DEBUG "libname: ${libname}")
  message(DEBUG "ARGN: ${ARGN}")

  message(DEBUG "Kokkos_ENABLE_CUDA: ${Kokkos_ENABLE_CUDA}")
  if(Kokkos_ENABLE_CUDA)
    set_source_files_properties(${ARGN} PROPERTIES LANGUAGE CUDA)
  endif()

  target_sources(${libname} PRIVATE ${ARGN})

  list(POP_BACK CMAKE_MESSAGE_INDENT)
endfunction(quicc_target_sources_kokkos)

