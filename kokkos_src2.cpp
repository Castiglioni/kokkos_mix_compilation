#include <Kokkos_Core.hpp>

int fct2_kokkos() {
    Kokkos::parallel_for(20, KOKKOS_LAMBDA(size_t i) {
            printf("Fct2: %i\n", i);
    });
    return 4;
}
