#include <iostream>

#include "kokkos_wrapper.hpp"
#include "non_kokkos_src1.h"
#include "non_kokkos_src2.h"
#include "kokkos_src1.h"
#include "kokkos_src2.h"

int main(int argc, char *argv[]) {
    kk_wrap::initialize(argc, argv);
    int val = fct1()+fct2()+fct1_kokkos()+fct2_kokkos();
    std::cout << "val=" << val << std::endl;
    kk_wrap::finalize();
    return 0;
}
