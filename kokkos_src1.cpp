#include <Kokkos_Core.hpp>

int fct1_kokkos() {
    Kokkos::parallel_for(10, KOKKOS_LAMBDA(int i) {
        printf("Hello %i\n", i);
    });
    return 3;
}
